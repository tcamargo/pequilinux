#!/bin/bash

set -e

USERID=${UID:-9001}
USERNAME="pequilinux"

useradd -s /bin/bash -u $USERID --no-create-home $USERNAME
export HOME=/home/$USERNAME

mkdir -p ${HOME}
cp -r /etc/skel/. /${HOME}

exec gosu $USERNAME "$@" 

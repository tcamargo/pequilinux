################################################################################
#
# initramfs
#
################################################################################
PEQUILINUX_INITRAMFS_VERSION = "6"
PEQUILINUX_INITRAMFS_SOURCE = 
PEQUILINUX_INITRAMFS_DEPENDENCIES = busybox host-cpio pequilinux-scripts
PEQUILINUX_INITRAMFS_INSTALL_IMAGES = YES

PEQUILINUX_INITRAMFS_WORKDIR = $(@D)/workdir

PEQUILINUX_INITRAMFS_NEEDED_APPS = awk cat chmod chown clear cttyhack dd df \
	dmesg fbset fdisk fsck grep gunzip head id ln ls losetup md5sum mkdir \
	mount mountpoint mv reboot rm rmdir sed setfont setsid sh sync stat tr \
	umount uname usleep

define PEQUILINUX_INITRAMFS_BUILD_CMDS
	mkdir -p $(PEQUILINUX_INITRAMFS_WORKDIR)/bin
	cp ${TARGET_DIR}/bin/busybox $(PEQUILINUX_INITRAMFS_WORKDIR)/bin
	chmod 4755 $(PEQUILINUX_INITRAMFS_WORKDIR)/bin/busybox
	$(TARGET_STRIP) $(PEQUILINUX_INITRAMFS_WORKDIR)/bin/busybox

	# applets
	$(foreach app,$(PEQUILINUX_INITRAMFS_NEEDED_APPS), \
		ln -sf busybox $(PEQUILINUX_INITRAMFS_WORKDIR)/bin/$(app)
	)

	mkdir -p $(PEQUILINUX_INITRAMFS_WORKDIR)/etc
	touch $(PEQUILINUX_INITRAMFS_WORKDIR)/etc/fstab
	ln -sf /proc/self/mounts $(PEQUILINUX_INITRAMFS_WORKDIR)/etc/mtab

	sed -e "s;@BUILDTIME@;$(PEQUILINUX_SYSTEM_DATE_TIME);g" \
		-e "s;@DISTRONAME@;pequilinux;g" \
		-e "s;@KERNEL_NAME@;KERNEL;g" \
		$(BR2_EXTERNAL_PEQUILINUX_PATH)/package/pequilinux/boot/pequilinux-initramfs/init \
		> $(PEQUILINUX_INITRAMFS_WORKDIR)/init
	chmod 755 $(PEQUILINUX_INITRAMFS_WORKDIR)/init

	cp $(TARGET_DIR)/usr/lib/pequilinux/functions \
		$(PEQUILINUX_INITRAMFS_WORKDIR)

	( cd $(PEQUILINUX_INITRAMFS_WORKDIR) && \
		find . -print0 | cpio --null --create --format=newc --owner 0:0 > $(@D)/initramfs.cpio \
	)
endef

define PEQUILINUX_INITRAMFS_INSTALL_IMAGES_CMDS
	cp $(@D)/initramfs.cpio $(BINARIES_DIR)/initramfs.cpio
endef

$(eval $(generic-package))

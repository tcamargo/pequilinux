################################################################################
#
# openrc-initscripts
#
################################################################################
OPENRC_INITSCRIPTS_VERSION = 11
OPENRC_INITSCRIPTS_SOURCE = 
OPENRC_INITSCRIPTS_DEPENDENCIES = openrc 

# new scripts and configuration files
OPENRC_INITSCRIPTS_INITD = connman dbus dropbear fsresize openntpd pipewire pipewire-media-session pipewire-pulse
OPENRC_INITSCRIPTS_CONFD = connman dropbear hwclock localmount openntpd

# runlevels
OPENRC_INITSCRIPTS_SYSINIT = fsresize
OPENRC_INITSCRIPTS_BOOT    = connman hwclock pipewire-media-session pipewire-pulse
OPENRC_INITSCRIPTS_DEFAULT = dropbear openntpd

define OPENRC_INITSCRIPTS_INSTALL_TARGET_CMDS
	# new init.d scripts
	$(foreach script,$(OPENRC_INITSCRIPTS_INITD), \
		cp $(BR2_EXTERNAL_PEQUILINUX_PATH)/package/pequilinux/boot/openrc-initscripts/initd/$(script).initd \
			$(TARGET_DIR)/etc/init.d/$(script); \
		chmod 755 $(TARGET_DIR)/etc/init.d/$(script); \
	)

	# new conf.d files
	$(foreach script,$(OPENRC_INITSCRIPTS_CONFD), \
		cp $(BR2_EXTERNAL_PEQUILINUX_PATH)/package/pequilinux/boot/openrc-initscripts/confd/$(script).confd \
			$(TARGET_DIR)/etc/conf.d/$(script);
	)

	# runleval boot
	$(foreach script,$(OPENRC_INITSCRIPTS_BOOT), \
		ln -sf /etc/init.d/$(script) \
			$(TARGET_DIR)/etc/runlevels/boot/$(script)
	)

	# runleval sysinit
	$(foreach script,$(OPENRC_INITSCRIPTS_SYSINIT), \
		ln -sf /etc/init.d/$(script) \
			$(TARGET_DIR)/etc/runlevels/sysinit/$(script)
	)

	# runlevel default
	$(foreach script,$(OPENRC_INITSCRIPTS_DEFAULT), \
		ln -sf /etc/init.d/$(script) \
			$(TARGET_DIR)/etc/runlevels/default/$(script)
	)

	cp $(BR2_EXTERNAL_PEQUILINUX_PATH)/package/pequilinux/boot/openrc-initscripts/rc.conf \
		$(TARGET_DIR)/etc/rc.conf
endef

$(eval $(generic-package))

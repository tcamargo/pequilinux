################################################################################
#
# pequilinux base system
#
################################################################################
# quotes in version will force package tp always build
PEQUILINUX_SYSTEM_VERSION = $(BR2_PACKAGE_PEQUILINUX_VERSION)
PEQUILINUX_SYSTEM_SOURCE =
PEQUILINUX_SYSTEM_INSTALL_IMAGES = YES

PEQUILINUX_SYSTEM_VERSION_STRIPED = $(call qstrip,$(BR2_PACKAGE_PEQUILINUX_VERSION))

PEQUILINUX_SYSTEM_GIT_VERSION = $(shell $(TOPDIR)/support/scripts/setlocalversion $(BR2_EXTERNAL_PEQUILINUX_PATH))
PEQUILINUX_SYSTEM_DATE_TIME = $(shell date -u "+%Y/%m/%d %H:%M:%S")
PEQUILINUX_SYSTEM_DATE = $(shell date -u "+%Y/%m/%d")

PEQUILINUX_SYSTEM_CFLAGS  = -mtune=$(GCC_TARGET_CPU)
PEQUILINUX_SYSTEM_LDFLAGS = -mtune=$(GCC_TARGET_CPU)
# platform specifics
PEQUILINUX_SYSTEM_BOARD_DIR := $(BR2_EXTERNAL_PEQUILINUX_PATH)/board/pequilinux/amlogic
# amlogic optimization flags
PEQUILINUX_SYSTEM_ARCH = amlogic.aarch64
PEQUILINUX_SYTEM_CFLAGS  += -march=armv8-a+crc+fp+simd -mno-outline-atomic
PEQUILINUX_SYTEM_LDFLAGS += -march=armv8-a+crc+fp+simd
PEQUILINUX_SYTEM_CXXFLAGS := $(PEQUILINUX_CFLAGS)

export PEQUILINUX_FULL_VERSION :=  pequilinux $(PEQUILINUX_SYSTEM_VERSION)

define PEQUILINUX_SYSTEM_BUILD_CMDS
	@echo "$(BR2_VERSION_FULL)"
	@echo "VERSION: $(PEQUILINUX_SYSTEM_VERSION)"
	@echo "TARGET_CFLAGS: $(TARGET_CFLAGS)"
	@echo "TARGET_LDFLAGS: $(TARGET_LDFLAGS)"
	@echo "PEQUILINUX_CFLAGS: $(PEQUILINUX_SYSTEM_CFLAGS)"
	@echo "PEQUILINUX_CXXFLAGS: $(PEQUILINUX_SYSTEM_CXXFLAGS)"
	@echo "PEQUILINUX_LDFLAGS: $(PEQUILINUX_SYSTEM_LDFLAGS)"
endef

define PEQUILINUX_SYSTEM_CREATE_SKELETON
	# create pequilinux skeleton
	mkdir -p $(TARGET_DIR)/flash
	mkdir -p $(TARGET_DIR)/overlay
	mkdir -p $(TARGET_DIR)/storage/.update
	mkdir -p $(TARGET_DIR)/storage/bios
	mkdir -p $(TARGET_DIR)/storage/roms
	mkdir -p $(TARGET_DIR)/storage/saves
	mkdir -p $(TARGET_DIR)/storage/screenshots
	mkdir -p $(TARGET_DIR)/storage/system/openrc
	mkdir -p $(TARGET_DIR)/storage/system/ssh
	touch $(TARGET_DIR)/storage/.please_resize_me
endef
PEQUILINUX_SYSTEM_POST_INSTALL_TARGET_HOOKS += PEQUILINUX_SYSTEM_CREATE_SKELETON

define PEQUILINUX_SYSTEM_INSTALL_PEQUILINUX_RELEASE
	# Create /etc/pequilinux-release
	( \
		echo "NAME=\"pequilinux\""; \
		echo "VERSION=$(PEQUILINUX_SYSTEM_VERSION_STRIPED)"; \
		echo "ID=\"pequilinux\""; \
		echo "VERSION_ID=$(PEQUILINUX_SYSTEM_VERSION_STRIPED)"; \
		echo "PRETTY_NAME=\"pequilinux $(PEQUILINUX_SYSTEM_VERSION_STRIPED) ($(PEQUILINUX_SYSTEM_ARCH))\""; \
		echo "HOME_URL=\"https://gitlab.com/tcamargo/pequilinux/\""; \
		echo "BUG_REPORT_URL=\"https://gitlab.com/tcamargo/pequilinux/-/issues\""; \
		echo "BUILD_ID=\"$(PEQUILINUX_SYSTEM_GIT_VERSION)\""; \
		echo "PEQUILINUX_ARCH=\"$(PEQUILINUX_SYSTEM_ARCH)\""; \
		echo "PEQUILINUX_BUILD_TIME=\"$(PEQUILINUX_SYSTEM_DATE_TIME)\""; \
		echo "BUILDROOT_VERSION=\"$(BR2_VERSION_FULL)\""; \
	) > $(TARGET_DIR)/etc/pequilinux-release
endef
PEQUILINUX_SYSTEM_POST_INSTALL_TARGET_HOOKS += PEQUILINUX_SYSTEM_INSTALL_PEQUILINUX_RELEASE

define PEQUILINUX_SYSTEM_INSTALL_PROFILE_D
	cp $(BR2_EXTERNAL_PEQUILINUX_PATH)/package/pequilinux/core/pequilinux-system/profile.d/* \
		$(TARGET_DIR)/etc/profile.d/
endef
PEQUILINUX_SYSTEM_POST_INSTALL_TARGET_HOOKS += PEQUILINUX_SYSTEM_INSTALL_PROFILE_D

define PEQUILINUX_SYSTEM_INSTALL_IMAGES_CMDS
	# copy board specific files
	cp $(PEQUILINUX_SYSTEM_BOARD_DIR)/extlinux.conf $(BINARIES_DIR)
	cp $(PEQUILINUX_SYSTEM_BOARD_DIR)/genimage.cfg $(BINARIES_DIR)

	# persist version and for image build
	echo $(PEQUILINUX_SYSTEM_VERSION) > $(BINARIES_DIR)/pequilinux.version
endef

$(eval $(generic-package))

################################################################################
#
# pequilinux scripts
#
################################################################################
PEQUILINUX_SCRIPTS_VERSION = 2
PEQUILINUX_SCRIPTS_SOURCE = 
 
define PEQUILINUX_SCRIPTS_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/usr/lib/pequilinux
	
	cp $(BR2_EXTERNAL_PEQUILINUX_PATH)/package/pequilinux/core/pequilinux-scripts/fsresize \
		$(TARGET_DIR)/usr/lib/pequilinux
	chmod 755 $(TARGET_DIR)/usr/lib/pequilinux/fsresize
	
	cp $(BR2_EXTERNAL_PEQUILINUX_PATH)/package/pequilinux/core/pequilinux-scripts/lsb_release \
		$(TARGET_DIR)/usr/lib/pequilinux
	chmod 755 $(TARGET_DIR)/usr/lib/pequilinux/lsb_release

	cp $(BR2_EXTERNAL_PEQUILINUX_PATH)/package/pequilinux/core/pequilinux-scripts/nologin \
		$(TARGET_DIR)/usr/lib/pequilinux
	chmod 755 $(TARGET_DIR)/usr/lib/pequilinux/nologin

	cp $(BR2_EXTERNAL_PEQUILINUX_PATH)/package/pequilinux/core/pequilinux-scripts/functions \
		$(TARGET_DIR)/usr/lib/pequilinux
endef

$(eval $(generic-package))
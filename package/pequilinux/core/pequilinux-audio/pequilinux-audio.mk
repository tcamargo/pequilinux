################################################################################
#
# pequilinux audio
#
################################################################################
PEQUILINUX_AUDIO_VERSION = 1
PEQUILINUX_AUDIO_SOURCE =
PEQUILINUX_AUDIO_DEPENDENCIES = pipewire 

define PEQUILINUX_AUDIO_INSTALL_TARGET_CMDS
	# pipewire-pulse policy
	mkdir -p $(TARGET_DIR)/etc/dbus-1/system.d
	cp $(BR2_EXTERNAL_PEQUILINUX_PATH)/package/pequilinux/core/pequilinux-audio/pulseaudio-system.conf \
		$(TARGET_DIR)/etc/dbus-1/system.d

	# pipewire-alsa
	mkdir -p $(TARGET_DIR)/etc/alsa/conf.d
	ln -sft $(TARGET_DIR)/etc/alsa/conf.d \
		/usr/share/alsa/alsa.conf.d/{50-pipewire,99-pipewire-default}.conf 
	install -Dm644 /dev/null $(TARGET_DIR)/etc/pipewire/media-session.d/with-alsa

	# pipewire-media-session config: disable dbus device reservation
	mkdir -p $(TARGET_DIR)/etc/pipewire/media-session.d
	cp $(BR2_EXTERNAL_PEQUILINUX_PATH)/package/pequilinux/core/pequilinux-audio/media-session.conf \
		$(TARGET_DIR)/etc/pipewire/media-session.d

	# # get rid of pulseaudio files
	# rm -rf $(TARGET_DIR)/etc/pulse
endef

$(eval $(generic-package))
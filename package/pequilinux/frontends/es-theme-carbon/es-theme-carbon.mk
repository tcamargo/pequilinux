################################################################################
#
# EmulationStation Carbon Theme
#
################################################################################
ES_THEME_CARBON_VERSION = b09973e0b0c589cb11fe772c169a6ff5d588b390
ES_THEME_CARBON_SITE = $(call github,RetroPie,es-theme-carbon,$(ES_THEME_CARBON_VERSION))
ES_THEME_CARBON_DEPENDENCIES = retropie-emulationstation 

define ES_THEME_CARBON_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/etc/emulationstation/themes/es-theme-carbon
	cp -a $(@D)/* $(TARGET_DIR)/etc/emulationstation/themes/es-theme-carbon
endef

$(eval $(generic-package))
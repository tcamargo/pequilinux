################################################################################
#
# Retropie EmulationStation
#
################################################################################

RETROPIE_EMULATIONSTATION_VERSION = c6bbd38d53d0d1a8929f01e32ebd4b72473c6422
RETROPIE_EMULATIONSTATION_SITE = https://github.com/RetroPie/EmulationStation
RETROPIE_EMULATIONSTATION_SITE_METHOD = git
RETROPIE_EMULATIONSTATION_LICENSE = MIT
RETROPIE_EMULATIONSTATION_GIT_SUBMODULES = YES
RETROPIE_EMULATIONSTATION_LICENSE = MIT, Apache-2.0
RETROPIE_EMULATIONSTATION_DEPENDENCIES = freetype libcurl libfreeimage rapidjson sdl2 sdl2_mixer vlc

# RETROPIE_EMULATIONSTATION_CONF_OPTS += -DCMAKE_CXX_FLAGS=-D$(call UPPERCASE,$(RETROPIE_SYSTEM_ARCH))
# RETROPIE_EMULATIONSTATION_CONF_OPTS += -DDISABLE_KODI=1

ifeq ($(BR2_PACKAGE_HAS_LIBGLES),y)
RETROPIE_EMULATIONSTATION_CONF_OPTS += -DGLES=ON
endif

ifeq ($(BR2_PACKAGE_HAS_LIBEGL),y)
RETROPIE_EMULATIONSTATION_CONF_OPTS += -DEGL=ON
endif

define RETROPIE_EMULATIONSTATION_RESOURCES
	$(INSTALL) -m 0755 -d $(TARGET_DIR)/usr/share/EmulationStation/resources/help
	$(INSTALL) -m 0644 -D $(@D)/resources/*.* $(TARGET_DIR)/usr/share/EmulationStation/resources
	$(INSTALL) -m 0644 -D $(@D)/resources/help/*.* $(TARGET_DIR)/usr/share/EmulationStation/resources/help
endef
RETROPIE_EMULATIONSTATION_POST_INSTALL_TARGET_HOOKS += RETROPIE_EMULATIONSTATION_RESOURCES

define RETROPIE_EMULATIONSTATION_INITSCRIPT
	mkdir -p /etc/init.d
	cp $(BR2_EXTERNAL_PEQUILINUX_PATH)/package/pequilinux/frontends/retropie-emulationstation/emulationstation.initd \
		$(TARGET_DIR)/etc/init.d/emulationstation
	chmod 755 $(TARGET_DIR)/etc/init.d/emulationstation
	ln -sf /etc/init.d/emulationstation \
		$(TARGET_DIR)/etc/runlevels/default/emulationstation
endef
RETROPIE_EMULATIONSTATION_POST_INSTALL_TARGET_HOOKS += RETROPIE_EMULATIONSTATION_INITSCRIPT

$(eval $(cmake-package))

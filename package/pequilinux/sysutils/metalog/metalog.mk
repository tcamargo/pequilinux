################################################################################
#
# metalog
#
################################################################################
METALOG_VERSION = metalog-20200113
METALOG_SITE = $(call github,hvisage,metalog,$(METALOG_VERSION))
METALOG_DEPENDENCIES = pcre openrc host-autoconf-archive host-pkgconf
METALOG_AUTORECONF = YES
METALOG_AUTORECONF_OPTS = --include=$(HOST_DIR)/share/autoconf-archive

define METALOG_CONF_FILES
		cp $(BR2_EXTERNAL_PEQUILINUX_PATH)/package/pequilinux/sysutils/metalog/metalog.initd \
			$(TARGET_DIR)/etc/init.d/metalog
		chmod 755 $(TARGET_DIR)/etc/init.d/metalog
		cp $(BR2_EXTERNAL_PEQUILINUX_PATH)/package/pequilinux/sysutils/metalog/metalog.confd \
			$(TARGET_DIR)/etc/conf.d/metalog
		ln -sf /etc/init.d/metalog \
			$(TARGET_DIR)/etc/runlevels/sysinit/metalog
endef

METALOG_POST_INSTALL_TARGET_HOOKS += METALOG_CONF_FILES

$(eval $(autotools-package))
#! /usr/bin/env bash

set -e

GENIMAGE_TMP="${BUILD_DIR}/genimage.tmp"
ROOTPATH_TMP="$(mktemp -d)"
PEQUILINUX_VERSION=$(cat ${BINARIES_DIR}/pequilinux.version)
RELEASE_DIR=${BINARIES_DIR}/release

echo "Creating ${PEQUILINUX_VERSION} installation image"

# Remove old build artifacts
rm -rf "${GENIMAGE_TMP}"
rm -rf "${RELEASE_DIR}"

# copy TARGET_DIR/storage to ROOTPATH_TMP to populate ext4 fs
rsync -av "${TARGET_DIR}/storage" "${ROOTPATH_TMP}"

# Setup release directory
mkdir -p "${RELEASE_DIR}/boot/extlinux" 
cp "${BINARIES_DIR}/extlinux.conf" "${RELEASE_DIR}/boot/extlinux"

echo "Copying kernel, ramdisk, rootfs, and device tree"
cp "${BINARIES_DIR}/Image"           "${RELEASE_DIR}/KERNEL"
cp "${BINARIES_DIR}/rootfs.squashfs" "${RELEASE_DIR}/SYSTEM"
cp "${BINARIES_DIR}/initramfs.cpio"  "${RELEASE_DIR}/INITRD"
cp "${BINARIES_DIR}"/*.dtb           "${RELEASE_DIR}"
cp "${BINARIES_DIR}"/boot.scr        "${RELEASE_DIR}"

( cd ${RELEASE_DIR};
	md5sum "KERNEL" > "KERNEL.md5"
	md5sum "SYSTEM" > "SYSTEM.md5"
	md5sum "INITRD" > "INITRD.md5"
)

# bootloader
echo "Preparing u-boot files"
dd if="${BINARIES_DIR}/u-boot.bin.sd.bin" of="${BINARIES_DIR}/u-boot1.bin" bs=1   count=444
dd if="${BINARIES_DIR}/u-boot.bin.sd.bin" of="${BINARIES_DIR}/u-boot2.bin" bs=512 skip=1

# Building
"${HOST_DIR}/bin/genimage" \
	--rootpath="${ROOTPATH_TMP}" \
	--inputpath="${RELEASE_DIR}" \
	--outputpath="${RELEASE_DIR}" \
	--config="${BINARIES_DIR}/genimage.cfg" \
	--tmppath="${GENIMAGE_TMP}"

# Cleaning-uo
rm -rf "${GENIMAGE_TMP}"
rm -rf "${ROOTPATH_TMP}"
rm -f "${RELEASE_DIR}/boot.vfat"
rm -f "${RELEASE_DIR}/storage.ext4"

# Release image

mv "${RELEASE_DIR}/pequilinux.img" "${RELEASE_DIR}/pequilinux-${PEQUILINUX_VERSION}.img"
gzip "${RELEASE_DIR}/pequilinux-${PEQUILINUX_VERSION}.img"
( cd ${RELEASE_DIR};
	sha256sum "pequilinux-${PEQUILINUX_VERSION}.img.gz" > "pequilinux-${PEQUILINUX_VERSION}.img.gz.sha256"
)

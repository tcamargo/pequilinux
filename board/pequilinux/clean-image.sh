#! /usr/bin/env bash

set -e

# remove sysv scriptsc
OPENRC_INITSCRIPTS_DISABLE="binfmt loopback mtab network netmount staticroute sysv-rcs"
rm -rf "${TARGET_DIR}/etc/init.d/S*"
rm -rf "${TARGET_DIR}/linuxrc"
# remove unwanted services
for script in ${OPENRC_INITSCRIPTS_DISABLE}; do
    rm -f ${TARGET_DIR}/etc/runlevels/boot/${script}
	rm -f ${TARGET_DIR}/etc/runlevels/sysinit/${script}
	rm -f ${TARGET_DIR}/etc/runlevels/default/${script}
	rm -f ${TARGET_DIR}/etc/init.d/${script}
	rm -f ${TARGET_DIR}/etc/conf.d/${script}
done

# make sure openrc can save cache
mkdir -p "${TARGET_DIR}/storage/system/openrc/cache"
ln -sf "/storage/system/openrc/cache" "${TARGET_DIR}/usr/libexec/rc/cache"

# override skeleton-init-openrc: /var/lock -> /run/lock
rm -rf "${TARGET_DIR}/var/lock"
ln -sf "../run/lock" "${TARGET_DIR}/var/lock"

# override ???: ntp needs it for drift file
rm -rf "${TARGET_DIR}/var/db"
ln -sf "../tmp" "${TARGET_DIR}/var/db"

# dropbear needs rw to create keys on first boot
ln -sf "/storage/system/ssh" "${TARGET_DIR}/etc/dropbear"

# link /etc/os-release to /etc/pequilinux-release
rm -rf "${TARGET_DIR}/etc/os-release"
rm -rf "${TARGET_DIR}/usr/lib/os-release"
ln -sf pequilinux-release  "${TARGET_DIR}/etc/os-release"

# change root dir to /storage
sed -i "s|^root:x:.*$|root:x:0:0:root:/storage:/bin/sh|g" "${TARGET_DIR}/etc/passwd"

# override busybox reboot and shutdown
ln -sf "/usr/libexec/rc/bin/reboot" "${TARGET_DIR}/usr/sbin"
ln -sf "/usr/libexec/rc/bin/shutdown" "${TARGET_DIR}/usr/sbin"
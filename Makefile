# SPDX-License-Identifier: MIT

PROJECT_DIR    := $(shell pwd)
DL_DIR         ?= $(PROJECT_DIR)/dl
OUTPUT_DIR     ?= $(PROJECT_DIR)/output
CCACHE_DIR     ?= $(PROJECT_DIR)/buildroot-ccache
LOCAL_MK	   ?= $(PROJECT_DIR)/pequilinux.mk
MAKE_JLEVEL    ?= $(shell nproc)

-include $(LOCAL_MK)

DOCKER_ORGANIZATION ?= discworld
BUILD_IMAGE_NAME    ?= pequilinux-build
BUILD_IMAGE_TAG     ?= latest

$(if $(shell which docker 2>/dev/null),,$(error "docker not found!"))
# $(if $(shell docker images -q $(DOCKER_ORGANIZATION)/$(BUILD_IMAGE_NAME):$(BUILD_IMAGE_TAG)),,$(error "docker not running or $(DOCKER_ORGANIZATION)/$(BUILD_IMAGE_NAME) image not available"))

TARGETS := $(sort $(shell find $(PROJECT_DIR)/configs/ -name '*' | sed -n 's/.*\/\(.*\)_defconfig/\1/p'))

vars:
	@echo "Supported targets:  $(TARGETS)"
	@echo "Project directory:  $(PROJECT_DIR)"
	@echo "Download directory: $(DL_DIR)"
	@echo "Build directory:    $(OUTPUT_DIR)"
	@echo "ccache directory:   $(CCACHE_DIR)"

%-supported:
	$(if $(findstring $*, $(TARGETS)),,$(error "$* not supported!"))

output-dir-%: %-supported
	@mkdir -p $(OUTPUT_DIR)/$*

ccache-dir:
	@mkdir -p $(CCACHE_DIR)

dl-dir:
	@mkdir -p $(DL_DIR)

docker-build-image:
	@docker build --pull --rm -f "tools/docker/build/Dockerfile" -t $(DOCKER_ORGANIZATION)/$(BUILD_IMAGE_NAME):$(BUILD_IMAGE_TAG) "tools/docker/build"

docker-get-build-image:
	@docker pull $(DOCKER_ORGANIZATION)/$(BUILD_IMAGE_NAME):$(BUILD_IMAGE_TAG)

shell:
	@docker run --init --rm -it \
		-v $(PROJECT_DIR):/pequilinux \
		-w /pequilinux \
		-e UID=$(shell id -u) \
		$(DOCKER_ORGANIZATION)/$(BUILD_IMAGE_NAME):$(BUILD_IMAGE_TAG)

uart:
	$(if $(shell which picocom 2>/dev/null),, $(error "picocom not found!"))
	$(if $(SERIAL_DEV),,$(error "SERIAL_DEV not specified!"))
	$(if $(SERIAL_BAUDRATE),,$(error "SERIAL_BAUDRATE not specified!"))
	$(if $(wildcard $(SERIAL_DEV)),,$(error "$(SERIAL_DEV) not available!"))
	@picocom $(SERIAL_DEV) -b $(SERIAL_BAUDRATE)

%-config: output-dir-%
	@docker run --init --rm -it\
		-v $(PROJECT_DIR):/pequilinux \
		-v $(OUTPUT_DIR)/$*:/$* \
		-e UID=$(shell id -u) \
		$(DOCKER_OPTS) \
		$(DOCKER_ORGANIZATION)/$(BUILD_IMAGE_NAME):$(BUILD_IMAGE_TAG) \
		make O=/$* BR2_EXTERNAL=/pequilinux -C /pequilinux/buildroot $*_defconfig

%-build: %-config ccache-dir dl-dir
	@docker run --init --rm -it \
		-v $(PROJECT_DIR):/pequilinux \
		-v $(DL_DIR):/pequilinux/buildroot/dl \
		-v $(OUTPUT_DIR)/$*:/$* \
		-v $(CCACHE_DIR):/home/pequilinux/.buildroot-ccache \
		-e UID=$(shell id -u) \
		$(DOCKER_OPTS) \
		$(DOCKER_ORGANIZATION)/$(BUILD_IMAGE_NAME):$(BUILD_IMAGE_TAG) \
		make -j$(MAKE_JLEVEL) O=/$* BR2_EXTERNAL=/pequilinux -C /pequilinux/buildroot $(CMD)

%-shell: output-dir-% dl-dir ccache-dir
	@docker run --init --rm -it \
		-v $(PROJECT_DIR):/pequilinux \
		-v $(DL_DIR):/pequilinux/buildroot/dl \
		-v $(OUTPUT_DIR)/$*:/$* \
		-v $(CCACHE_DIR):/home/pequilinux/.buildroot-ccache \
		-e UID=$(shell id -u) \
		$(DOCKER_OPTS) \
		-w /$* \
		$(DOCKER_ORGANIZATION)/$(BUILD_IMAGE_NAME):$(BUILD_IMAGE_TAG)

%-clean: output-dir-%
	@$(MAKE) $*-build CMD=clean

%-webserver: output-dir-%
	$(if $(wildcard $(OUTPUT_DIR)/$*/images/release/*),,$(error "$* not built!"))
	$(if $(shell which python 2>/dev/null),,$(error "python not found!"))
	@python3 -m http.server --directory $(OUTPUT_DIR)/$*/images/release

%-flash: %-supported output-dir-%
	$(if $(DEV),,$(error "DEV not specified!"))
	$(if $(wildcard $(DEV)),,$(error "$(DEV) doesn't exist!"))
	@gzip -dc $(OUTPUT_DIR)/$*/images/release/pequilinux-*.img.gz | sudo dd of=$(DEV) bs=5M status=progress
	@sync

%-pkg:
	$(if $(PKG),,$(error "PKG not specified!"))
	@$(MAKE) $*-build CMD=$(PKG)

%-menuconfig:
	@$(MAKE) $*-build CMD=menuconfig

%-tail: output-dir-%
	@tail -F $(OUTPUT_DIR)/$*/build/build-time.log

%-cleanbuild: %-clean %-build
	@echo
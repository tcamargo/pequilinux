# pequilinux

pequilinuxis a 'Just enough OS' Linux distribution for the famous EmulationStation frontend and a simple Retroarch core. Extra emulators and programs must be provided by external packages. It's built by taking the best ideas from many linux distributions. Such as [Batocera.linux](https://batocera.org/), [Retropie](https://retropie.org.uk/), [RetroELEC](https://github.com/escalade/RetroELEC), [LibreELEC](https://libreelec.tv/), and [Gentoo](https://www.gentoo.org/).

**License & Copyright**

pequilinux original code is released under the [MIT License](licensehttps://opensource.org/licenses/MIT).

External software and some files used in the distribution might be subject to different licenses. For a complete copyright list please checkout the source code to examine license headers.
